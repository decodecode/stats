# Stats

	module.exports=stats=

		# @returns collected stats.
		collect:(fn)->
			stats.scopes?=[] # Ensure initialized.
			stats.scopes.push {} # New isolated scope.
			fn()
			stats.scopes.pop()

		# @returns list of keys of only non-zero counters in given scope.
		non_zero_counters:(s)->
			(Object.keys s).sort().filter (k)->
				(typeof s[k] isnt 'number') or s[k] isnt 0

		inc:(counter,i=1)->
			cs=stats.scopes # Shorthand.
			if cs.length is 0 then cs.push {} #? Throw if no scopes in stack, or ensure always has one?
			cs[cs.length-1][counter]?=0
			cs[cs.length-1][counter]+=i

		#? Replace set with Proxy mechanism so can directory set/get any properties directly on stats. Prefix "reserved" methods with "_"?
		set:(counter,v)->stats.scopes[stats.scopes.length-1][counter]=v

		# @returns stringified given stats; undefined if empty.
		summary:(options,scope)-> #?// Order of params sucks! options obj instead?
			unless scope? then scope=options;options={} #?// Yuck
			#?//options=Object.assign filter_zeroes:yes,colon:' ',delimiter:', ',options
			{filter_zeroes,colon,delimiter}=options
			if filter_zeroes then cs=stats.non_zero_counters scope
			else cs=(Object.keys scope).sort()
			if cs.length
				'Stats: '+("#{k}#{colon ?' '}#{scope[k]}" for k in cs).join delimiter ?', '
