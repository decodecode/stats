# Stats
> Wrapper (decorator-like) to collect stats (counters, flags) per function call.

# TL;DR

	stats=require 'stats'
	console.log stats.summary stats.collect ->
		stats.inc 'counter'
	# => Stats: counter 1

## Features

(See [test/tests.coffee].)

## Installation

1. Build
1. Publish

## Test

Run server-side tests:

	$ npm test

(Will rebuild automatically — see "pretest" script in package.json.)

## Guide
## Community
## Release notes

### Next, currently in development

…

### 0.1.0, deployed 2018-08-12

- It works.
