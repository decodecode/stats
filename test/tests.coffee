# Tests.
expect=require 'expect.js'
stats=require '..'

describe 'Unit tests',->
	it 'should just work',->
		expect true
		.to.be true

describe 'Stats',->
	it 'should collect stats, increment counters, and return summary',->
		expect stats.summary stats.collect ->
			stats.inc 'foo'
			stats.inc 'bar',17
		.to.be 'Stats: bar 17, foo 1'

	it 'nested…',->
		expect stats.summary stats.collect ->
			stats.inc 'foo'
			expect stats.summary colon:'=',stats.collect ->
				stats.inc 'nested'
				stats.inc 'foo'
			.to.be 'Stats: foo=1, nested=1'
			stats.inc 'bar'
		.to.be 'Stats: bar 1, foo 1'

	it 'non-zero…',->
		expect stats.summary filter_zeroes:no,stats.collect ->
			stats.inc 'foo'
			stats.inc 'foo',-1
		.to.be 'Stats: foo 0'
