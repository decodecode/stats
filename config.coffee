## config.coffee
# Configuration (internal).

# (sync-const-skel config.coffee 0.11.2) scrapped almost everything; stuff to keep Cakefile from breaking?

env=process.env # Shorthand.

# Namespace env variables.
prefix='APP_' #?// Scrap!

# DRY some flags.
is_dev=env.NODE_ENV is 'development'
is_prod=env.NODE_ENV is 'production'

# Utilities.
convert_if_bool_str=(s)->switch s
	when 'on','true','yes' then true
	when 'false','no','off' then false
	else s
export_properties=(o)->
	for own k,v of o
		envvar=prefix+k.toUpperCase()
		exports[k]=if envvar of env then convert_if_bool_str env[envvar] else v

# Configuration to export.
export_properties
	# (Alphabetically, mostly.)
	# DEBUG defaults. #?// Having second thoughts about this mechanism: scrap it?
	debug_enable:'server,ldb,socket.io,body-parser' #/
	#?//rsync_dest:ssh_account+':'+production_path #?// Rename deploy_path? Used other than for Rsync.
	#?//ssh_account:ssh_account #?// Rename "credentials"?
