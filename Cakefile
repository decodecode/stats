## Cakefile

# (sync-const-skel Cakefile 0.29.0) config related issues?

env=process.env # Shorthand.
is_dev=env.NODE_ENV is 'development'
is_prod=env.NODE_ENV is 'production'

# Dependencies.
require 'stringly' #?// Need polyfill for endsWith. #? Avoid if possible; less dependencies, particularly special ones like this.
#?//promise=require 'promise' #?// Convert all sync to promisified async? But expecting marginal gains? But, nu, this. is. Node!
#?//fs=require 'fs'
fse=require 'fs-extra' # (Drop in replacement for Node's built-in fs.)
glob=require 'glob' #?// Replace with globby, promisified!? Yeah, but ES6  crap.
path=require 'path'
{exec,spawn}=require 'child_process' #?// Won't need when purely CS!? Nu, there's always more stuff out there in Unix.
CSON=require 'cson-parser'
require 'colors'
delog=require 'delog'
_=require 'lodash' #?// Meh?
coffee=require 'coffee-script' # ;o)
stylus=require 'stylus'

# Options (shared by all tasks).
option '-v','--verbose','Say what you mean'
option '-o','--browser','Open in browser, too' #?// Replace "--browser start" with just "browser"?
option '-c','--clean','Rebuild everything'
#?//option '-s','--styleguide','Styleguide'
#?//option '-d','--dirty','Even if repo is dirty'
#?//option '-l','--lint','You will be assimilated'
#?//option '-s','--stats','Stats' #?// Count/summarize files handled, as indication of activity, but less than verbose. Would've used verbose with parameter, except Cake doesn't support that.
option '-q','--quiet','Quiet' # Suppress informative output. #?// Assumes default output. --no-stats instead?
#?//... Stop running server before deploying, using PM2? Or, if required, add "stop" action? Separate commands more expressive.

# Misc globals.
stats=
	zero_counters:->@fresh=@written=@copied=@deleted=0 #?// Ugh, Q&D? #?// Instead of "++", inc() with detection will obviate this!
	is_non_zero:->@fresh+@written+@copied+@deleted isnt 0 #?// Smells!
	non_zero_counters:->
		self=@
		(Object.keys @).filter (k)->(typeof self[k] is 'number') and self[k] isnt 0
stats.zero_counters()

# Configuration.
config=require './config' #?// Cancel! Or is this our wrapper over env?
npm=require './package.json'
bower=require './bower.json'

# env.DEBUG.
(delog 'debug') 'DEBUG=',env.DEBUG?.blue
(delog 'config') ('\n\t'+CSON.stringify config,undefined,'\t'
	.split '\n'
	.join '\n\t')

# Tasks.
task 'start','Compile/preprocess and run local web server',(o)->global_options o,->build ->serve()
task 'clean','rm -rf dist/',(o)->global_options o,->clean ->grin()
#?//task 'install','Install dependencies from NPM, Bower, etc.',(o)->global_options o,->install ->grin()
task 'build','Compile/preprocess CoffeeScript/Stylus, etc',(o)->global_options o,->build ->grin()
task 'deploy','Build for and deploy to production',(o)->global_options o,->
	# Require setting env to either staging or production.
	unless env.NODE_ENV in ['production','staging'] then return console.error 'Error:'.red.inverse,'must set NODE_ENV to either production or staging.'
	build ->deploy ->restart ->grin() #?// Lib: don't restart, or use publish instead of deploy! #?// Detect whether app or lib somehow: env, or sources.
#?// task 'bump','Bump build version',(o)->global_options o,->bump
#?//... Add test task, instead of npm test, to verify not in production mode, and to run CS based tests too, maybe with Testem.
#?//... Watch! #?// Deploy means not watching, so maybe different build task?
task 'install','Install (and build) dependencies',(o)->global_options o,->install ->grin()
#?//task 'styleguide','Generate styleguide',(o)->global_options o,->build_styleguide ->grin() #?// Or make part of build_styl!?
#?// Add "json" task to reformat JSON files; but, Cake doesn't support CLI arguments? So redirect stdin?
task 'json','Format JSON for legibility (stdin→stdout)',(o)->global_options o,->json()
#?//task 'publish','Publish to package repository'
#?//task 'local','Publish to local directory' #?// Or publish --local?

# Utilities.
announce=(t,next)->
	unless options.quiet then console.log t.blue.inverse
	next?()
global_options=(o,next)->
	global.options=o # Expose globally, because Cake passes options to tasks only.
	# Default verbosity.
	if options.verbose then for f in 'compile fresh'.split ' ' #?// Confusing, so cancel, use "DEBUG=compile,fresh …" instead?
		delog.flags[f]=true
	# Quiet?
	unless options.quiet
		delog.flags.stats=yes
	next?()
#?//... Doc all these.
#?// Promisify!
refresh=(src,dest,cb)->
	if fresh src,dest then stats.fresh++; (delog 'fresh') dest
	else cb src,dest
fresh=(src,dest)->
	#?//… Be nice if src could be a list, I guess? When converting to async, use Promise.all.
	mtime=(f)->
		try
			(fse.statSync f).mtime.getTime() #?// Doesn't seem to work across symlinked directories?
		catch #?// File doesn't exist?
			0
	(mtime src)<=(mtime dest)
# Summary: print stats.
summarize=-> #?// Replace with scoped wrapper mechanism!
	#?// Lazy ass version; should only print non-zero counters. Prefix "files" per counter, or globally? #?// Why .blue?
	cs=stats.non_zero_counters()
	if cs.length
		(delog 'stats') 'Files:',("#{stats[counter]} #{counter}" for counter in cs).join ', '
		for c in cs
			do ->stats[c]=0
#?// glob-array: WTF? Another module for a one-liner?
# @param paths=space delimited patterms. Does not recurse. Returns paths, not just file names.
multi_glob=(paths)-> #?// Weird, rename find_files!?
	#?//... Convert into glob-fs MW?! No, seems they only post-process matched files.
	[].concat (glob.sync p for p in paths.split ' ')...
# Run in a shell.
shell=(cmd,next)->
	#?// Use promises instead of "next" CB, and chaining(!), so caller decides how to continue?
	(delog 'shell') 'sh'.cyan,cmd.split('\n').map((s)->s.trim()).join(';')
	# (NB: uses /bin/sh, which is Dash on Xubuntu; add shell:'/bin/bash' for Bash.)
	exec cmd,(err,stdout,stderr)->
		if err then console.error 'Error:'.red.inverse,err.stack #?// {err,stdout,stderr} #?// err seems always contains entire stderr, so don't dup!? Maybe unless stdout empty, or stderr appears in err?
		else
			if stdout then console.log stdout
			# Don't swallow warnings.
			if stderr then console.error stderr.red
			next?()
# Run process, forked, not in shell.
launch=(cmd,options=[],next)->
	(delog 'shell') 'spawn'.cyan,cmd,options,'...'
	app=spawn cmd,options
	app.stdout.pipe process.stdout
	app.stderr.pipe process.stderr
	app.on 'exit',(status)->
		if status is 0 then next?() #?// Must exist?
		else console.error 'Error:'.red.inverse+" exit code #{status} from #{cmd}"
## Green status.
grin=(next)->console.log ':-)'.green.inverse; next?()

## Install (and build) dependencies.
#?//... Run npm install and bower install? Detect when needs to update dependencies?
install=(next)->announce 'Installing',-> #?// Never used?
	# Post-process dependencies.
	#?//config.postinstall()
	#?//exports.postinstall=->
	# Build Coffee.
	for src,dest of bower.constitution.deps_coffee
		compile_coffee map:is_dev,src,dest
	###?// Combine. #?// Don't.
	#?// Promise.all!!!
	for target,src of bower.constitution.deps_combine
		fse.removeSync target
		fse.ensureDirSync path.dirname target
		shell """cat #{src} > #{target} || exit 102""",->
			(delog 'write') "combined #{target}"
	###
	summarize()
	next?() #?//...

## Copy dependencies and assets.
deps=(next)->announce 'Dependencies',->
	#?//... Promisify instead of sync and exceptions!
	try
		#?// Move this to "clean"? Already does that. I mean, redundant if only copies newer!
		#?//fse.emptyDirSync 'dist/client/lib'
		_.assign bower.constitution.deps,npm.constitution.deps
		if is_dev then _.assign bower.constitution.deps,bower.constitution.deps_dev,npm.constitution.deps_dev
		#?// Promisify and loop instead! Or, use Promise.all instead of loop!
		for own dest,paths of bower.constitution.deps #?// Ugly, copy all to local dict instead.
			(delog 'mkdir') 'dist/%s',dest
			fse.ensureDirSync "dist/#{dest}" #?// mkdirpSync or ensureDirSync?
			for p in multi_glob paths
				t="dist/#{dest}/#{path.basename p}"
				if fresh p,t then (delog 'fresh') 'fresh %s',t
				else
					(delog 'copy') '%s -> %s',p,t
					#?// Why copy instead of symlink as with eg images? Because want to maybe uglify them? Where that?
					fse.copySync p,t,clobber:yes,dereference:yes,preserveTimestamps:yes #?// Possible bug: doesn't fail when source doesn't exist? Yes, verified bug!
					stats.copied++
		#?// .done ->
	catch err
		console.error 'Error:'.red.inverse,err.stack
		return # Only continue if no exceptions. #?// Really? No, only run or deploy, but builds should be in parallel? Convert to promises, then can async all/any.
	summarize() #?// Replace with scoped wrapper mechanism!
	next?()

## Format JSON files.
# Pipes stdin→stdout; NB: can't overwrite source file — must rename and then remove.
# Useful for: package.json, bower.json.
#?// coffee -e "(require 'fs').writeFileSync 'package.pretty.json',JSON.stringify (require './package.json'),undefined,'\t'"
#?// coffee -e "console.log JSON.stringify (require './package.json'),undefined,'\t'" > pj
#?// coffee -e "console.log (require 'json-stable-stringify') (require './package.json'),space:'\t'" > pj
#?//... Also, sort maps and lists alphabetically, except root map — sort in convensional order?
json=->undefined

## Clean dist/ to force rebuilding everything.
clean=(next)->announce 'Cleaning',->
	try
		(delog 'remove') 'emptyDirSync dist'
		fse.emptyDirSync 'dist'
		(delog 'remove') 'removeSync .last-env'
		fse.removeSync '.last-env'
		stats.deleted+=2 #?// No indication if existed and actually removed?
	catch err
		console.error 'Error:'.red.inverse,err.stack
		return # Only continue if no exceptions.
	summarize()
	next?()

## Finish preparing distribution.
# Copies files that don't require building; dist/ contains only what's needed at runtime.
dist=(next)->announce 'Distribution',->
	try
		fse.ensureDirSync 'dist'
		# Server-side deliverables (excluding dependencies, which are copied by "deps"). #?// Why just SS?
		ff='stats.*coffee config.*coffee *.json server/*.*coffee' #?// Default files? But some don't exist, sometimes?
		if yes #?//
			ff+=' test/mocha* test/*.*coffee' #?// And lots more? #? Horrible string concatenation.
		for f in multi_glob ff
			dst=if f.startswith 'server/' then path.basename f else f #?// Don't strip path!!! Except server/! #? Yuck!!! #?// startsWith!!!
			if fresh f,"dist/#{dst}" then (delog 'fresh') 'fresh %s',f #?// DRY
			else
				(delog 'copy') 'copySync %s',f
				fse.copySync f,"dist/#{dst}"
				stats.copied++
		# Client-side.
		fse.ensureDirSync 'dist/client' #?// Don't, encapsulte in copying instead.
		# Images. (Symlinks are cheaper than copying files: development runs off of dist/ just fine; deploy to production will copy the referents, not symlinks.) #?// Split? Util? #?// Rsync will deploy copy, but cp won't — so using symlinks too risky?
		(delog 'copy') 'symlinkSync ../../client/image'
		if fse.existsSync  'client/image'
			fse.ensureSymlinkSync '../../client/image','dist/client/image' # (symlinkSync fails if target exists: [Error: EEXIST, file already exists 'dist/client/image'].)
		# PM2 app config.
		#?// This is a build step, why under "dist"? Own function? Because never need to build it during dev.
		if config?.monitor is 'pm2' and config?.pm2_json #?// Redundant flag? #?// Why guard config? So can shrink config by omitting unused keys.
			#?// fse.outputJson instead?
			fse.writeFileSync 'dist/ecosystem.json',JSON.stringify require './ecosystem.coffee' #?// .litcoffee? CSON?
	catch err
		console.error 'Error:'.red.inverse,err.stack #?// Add trace, or something, because "Error: relative srcpath does not exist" quite useless!?
		return # Only continue if no exceptions.
	summarize()
	next?()

## Build everything and prepare distribution.
build=(next)->announce 'Building',->
	console.log 'Target NODE_ENV is %s',env.NODE_ENV
	#?//promise.resolve if options.clean then clean() else undefined
	#?//new promise (resolve,reject)->
	#?//.done ->
	#?// Can't mix promises with Cake's chaining model, must rewrite them all!
	#?// Ugly logic for just cleaning: simplify, or hide! Also, no single place to simple log "not cleaning — incremental".
	# Clean if forced, or previous build was for a different env.
	if options.clean then clean()
	try
		(delog 'read') 'readFileSync .last-env'
		last=(fse.readFileSync '.last-env','utf8').trim()
	catch err
		# .last-env not existing means already clean.
		last=''
	if last and last isnt env.NODE_ENV
		console.log 'NODE_ENV changed: forcing clean'.red.inverse
		clean()
	try
		(delog 'write') '.last-env'
		fse.writeFileSync '.last-env',env.NODE_ENV
		stats.written++
	catch err
		console.error 'Error:'.red.inverse,err.stack
		return # Only continue if no exceptions.
	summarize()
	deps ->build_client ->build_styl ->build_templates ->dist next

global.compile_coffee=(opts,src,dst)-> #?// global? Yuck, don't. Why? I forget. #?// dst is directory. Deal with extensions separately, outside? And sourcemap?
	(delog 'compile') src
	f=path.basename src # File name.
	#?//n=path.basename src #?//,'.coffee' # Name without extension. #?// Doesn't strip ".litcoffee"!
	n=f.replace /\.(lit)?coffee/g,''
	code=fse.readFileSync src,'utf8'
	# (Coffee will throw on error.)
	{js,v3SourceMap}=coffee.compile code,
		literate:f.endsWith '.litcoffee'
		sourceMap:yes
		filename:f
		generatedFile:n+'.map'
		sourceFiles:[f]
	(delog 'write') dst
	fse.ensureDirSync dst
	fse.writeFileSync "#{dst}/#{n}.js",js
	stats.written++
	# Sourcemap.
	#?//d=if env.NODE_ENV is 'development' then '--map' else '' #?// SM been broken in CS for years!!!
	#?// Change directory so sourcemap URLs don't contain path. #?// Seems broken: browser looks for eg GET /static/unittest.js.map, or GET /client/completion.coffee 404 4ms !!!
	if is_dev
		(delog 'map') "#{dst}/#{n}.map"
		fse.writeFileSync "#{dst}/#{n}.map",v3SourceMap
		stats.written++
		# If dev, distribute source too, for sourcemap.
		(delog 'copy') src
		fse.copySync src,"#{dst}/#{f}",preserveTimestamps:yes
		stats.copied++

# Compile CoffeeScript into uglified JavaScript.
build_client=(next)->announce 'CoffeeScript',->
	try
		# Target directories. #?// Check per destination file instead, so won't need to specify!
		(delog 'mkdir') 'dist/test/client'
		#?//fse.mkdirpSync 'dist/client' #?// Or ensureDirSync?
		fse.ensureDirSync 'dist/test/client' #?// No, actually should build from test/client/ to dist/client/test/.
		# Client-side (in-browser) unit tests.
		#?//fse.ensureDirSync 'dist/client/test'
		cstests=if not is_prod then ' client/test/*.*coffee' else '' #?// Ugh. Move to config? No, drive scaffolding with maps, but in Cakefile… or maybe PJ/BJ, separately from deps?
		ff=multi_glob 'client/*.*coffee'+cstests #?// Convert to array, because space delim ugly? "".split /\s+/, [].concat …
		#?//... Fix testing, which didn't use to run off dist!?
		for p in ff
			#?//t=p.slice 0,-'.coffee'.length # Target, including path, without extension.
			t=p.replace /\.(lit)?coffee/g,''
			#?// Freshness should depend on other files, too!!! Sometimes: config.coffee, ./*.json, i18n… Routine? But, can't really tell, so manually "clean" instead?
			refresh p,"dist/#{t}.js",->compile_coffee map:(is_dev),p,"dist/#{path.dirname p}"
	catch err #?// Promisify instead!
		console.error 'Error:'.red.inverse,err.stack
		return # Only continue if no exceptions.
	summarize() #?// Replace with scoped wrapper mechanism!
	next?()

	###?//
	# Combine?
	#?// No, don't combine/bundle, in anticipation of HTTP2!!! And proper require loader!!!
	#?// Production: just uglify/pack!
	#?// Don't bundle CS unit tests!
	fse.removeSync 'dist/client/app.js'
	shell """
	cat dist/client"/"*.js > dist/client/app.js || exit 102
	""",->
		(delog 'write') 'combined into app.js'
	# Client-side (in-browser) unit tests.
	#?// Merge this with block above! Change mechanism to be filelist based, or refactor routines.
	#?// Don't bundle CS unit tests!
	cd test/browser/
	target="../../dist/client/test/"
	f="unittest.coffee"
	if [ "$NODE_ENV" != 'production' ] #?// UT in staging?!!!
	then
		coffee --compile #{d} --output "$target" "$f" || exit 103
		[ '#{v}' = '--verbose' ] && echo '  compiled' $f
		# If dev, distribute source too, for sourcemap.
		cp #{v} "$f" "$target"
	fi
	cd - > /dev/null
	###

# Preprocess Stylus into CSS.
build_styl=(next)->announce 'Stylus',->
	#?//try # Promisify instead!
	for p in multi_glob 'client/*.styl' #?// Read from config (PJ and B) as target:sources; see others. Dist Stylus includes from server/ to ./, and client/ to client/. Tests?
		base=path.basename p,'.styl' # Strip both dir and extension.
		target="dist/client/#{base}.css"
		# Fresh?
		#?//... Stylus can introspect and report dependencies (imports).
		if fresh p,target
			(delog 'fresh') p
		else
			(delog 'compile') p
			code=fse.readFileSync p,'utf8'
			#?//... Add https://github.com/rossPatton/stylint
			# Compile to CSS.
			stylus code
			#?// Rename .min.css when compressed? Why?
			.set 'filename',target # Stylus wants it for error reporting.
			.include 'node_modules'
			#?//.use
			#?//… Map!?
			.render (err,css)->  #?//... CB, or promisify it?
				#?// !!! Promisify and wait for all to be written.
				#?//... handle errors!
				if err then throw err #?//unless err
				fse.writeFileSync target,css
				stats.written++
	#?// Sourcemaps broken!!!: GET /client/taskman.styl 404 1ms
	#?// Rename .min.css when compressed? Why?
	summarize() #?// Replace with scoped wrapper mechanism!
	next?()
	###?// No client side stylesheets, yet?
	d=if is_dev then '--sourcemap' else '--compress'
	v=if options.verbose then '--verbose' else ''
	if fse.existsSync 'client/spa.styl' #?// Temporary hack!
		shell """cd client
		f="spa.styl" #?// Don't!!!
		if [ ! "$f" -ot "../dist/client/${f%.styl}.css" ] #?// Oy, broken because uses imports, must depend on all of them!!! And partials!
		then
			stylus --use autoprefixer-stylus --include ../node_modules #{d} --out ../dist/client/ "$f" || exit 104
			# If dev, distribute source too, for sourcemap.
			[ "$NODE_ENV" = 'development' ] && cp #{v} "$f" ../dist/client/
		else
			[ '#{v}' = '--verbose' ] && echo '  fresh' ${f%.styl}.css
		fi
		cd ..""",next
	else next?()
	###

# Render HTML from Double Macchiato templates.
build_templates=(next)->announce 'Templates',->
	try
		for p in multi_glob 'template/*.html.*coffee' #?//… Allow untraslated, ie without ".-.", too!
			n=path.basename p,'.coffee' # Strip both dir and extension; eg: spa.-.html. #?// .litcoffee
			# Repeat per language, for i18n support, if file name has placeholder for language.
			#?//d=path.dirname p #?// Overkill, since only single source directory so far?
			unless /\.-\./g.test n #?// Ugly, but "in" failed me?
				if fresh p,"dist/#{n}" then (delog 'fresh') n
				else
					# Render to HTML.
					(delog 'compile') n
					fse.writeFileSync "dist/#{n}",require "./#{p}"
					stats.written++
			else
				for lang in config.translations.split ' ' #?// Require translations to be specified!?
					t=n.replace '.-.',".#{lang}." # Replace language; eg: spa.en.html.
					# Fresh? #?// Also compare against config.coffee and package.json!!!? No, user should clean.
					if fresh p,"dist/#{t}" then (delog 'fresh') t
					else
						# Render to HTML.
						(delog 'compile') t
						#?!!!// Compilation errors not caught? Verify methodically!
						# ("./" for Node's require to include CWD. Bash: either '$f' or \'$f\' works; similarly for \{\}.)
						fse.writeFileSync "dist/#{t}",(require "./#{p}") lang
						stats.written++
	catch err
		#?// On failure, should remove target (empty proly), because it has newer timestamp, prevents recompiling!	#?// Bug!!! Leave empty target file if compilation fails!
		#?//... if n? then removeSync "dist/#{n}"
		console.error 'Error:'.red.inverse,err.stack
		return # Only continue if no exceptions.
	summarize() #?// Replace with scoped wrapper mechanism!
	next?()
build_styleguide=(next)->announce 'Styleguide',->
	next?()

#?// Bump build number (for versioning).
#?// bump=(next)->shell 'B=$((`cat BUILD`+1)); echo -n $B > BUILD; cat BUILD',next #?// !!! Edit package.json instead!

# Serve development app.
serve=(next)->announce 'Serving',->
	#?//... ensure in development!!!
	#?// Race condition: sometimes browser tab up before server listening, but, can't be (conveniently) avoided?
	if options.browser then shell "xdg-open http://#{config?.localhost_alias ?'localhost'}:#{config?.port}" #?// config? Copy env prefixing here? #?// Omit ":" unless port. #?// sensible-browser? #?// Ugly! Hide all this URL thing in a util.
	process.chdir 'dist' #?//
	launch 'coffee',['server.coffee'],next #?// .litcoffee!
	#?//... Add watch!

## Deploy to production/staging.
#?// Tag repo automatically with timestamp every time deployed, because no history on WF!!!
#?// !!! Refuse to deploy if not committed!
#?// Verify deploying from master! Cf ~/.bash-git-prompt/gitstatus.sh
deploy=(next)->announce 'Deploying',->
	#?//... Stop running server before deploying, using PM2? No — if required, add "stop" action.
	unless options.quiet then v='--verbose' #?// !!! v=if options.verbose then '--verbose' else ''
	# (--copy-links: transform symlink into referent file/dir.)
	shell "rsync --copy-links --recursive --compress #{v ?''} --checksum dist/ #{config.rsync_dest}",next # NB: trailing slash required after "dist/"! #?// Remove deleted. #?// Verify rsync_dest specified!?
## Restart (PM2 or WebFaction).
restart=(next)->switch config.monitor
	when 'pm2' then announce 'Restarting via PM2',->
		shell """
		echo "NODE_ENV=$NODE_ENV"
		[ "$NODE_ENV" = 'production' ] || exit 106 #?// echo explanation, too!
		#?//cd #{config.rsync_dest} #?//pushd #{config.rsync_dest} #?// Why? Ah, don't need it, because PM2 remembers CWD, and other stuff, apparently?
		pm2 startOrRestart --env production #{config.rsync_dest}/ecosystem.json
		sleep 5 #?// Too soon?
		#?// Print status instead of logs? Or just verify it's "online"?
		#?//tail ~/.pm2/logs/tm-*.log #?// Uh? Kindda useless.
		#?//popd #?// Bash internal, no Dash!
		#?//cd -
		""",next
	when 'webfaction' then announce 'Restarting on WF',->
		shell 'ssh #{config.ssh_account} "cd #{config.production_path}; stop; start; sleep 5; tail ~/logs/user/#{config.app}.log"',next
	else next?()
